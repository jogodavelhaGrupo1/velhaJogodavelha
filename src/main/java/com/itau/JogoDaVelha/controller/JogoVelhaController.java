package com.itau.JogoDaVelha.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.JogoDaVelha.model.JogoVelha;
import com.itau.JogoDaVelha.model.Jogada;
import com.itau.JogoDaVelha.model.ResultadoJogada;
import com.itau.JogoDaVelha.model.TabuleiroVelha;

@Controller
@CrossOrigin
public class JogoVelhaController {
	
	JogoVelha jogoVelha;
	
//	@RequestMapping("/tabuleiro")
	@RequestMapping(path = "/tabuleiro", method = RequestMethod.POST)
	public @ResponseBody
	boolean iniciarJogo() {
		jogoVelha = new JogoVelha();
	    return true;
	}
	
    @RequestMapping(path = "/jogada", method = RequestMethod.POST)
    public @ResponseBody
    ResultadoJogada jogar(@RequestBody Jogada jogada) {
    	ResultadoJogada resultado = new ResultadoJogada();
        jogoVelha.jogar(jogada.x, jogada.y);
        resultado = jogoVelha.getResultadoJogada();
        return resultado;
    }
    
    @RequestMapping(path = "/casas", method = RequestMethod.POST)
    public @ResponseBody
    TabuleiroVelha resultado() {
    	TabuleiroVelha tabuleiro = new TabuleiroVelha();
        tabuleiro.setTabuleiro(jogoVelha.getCasas());
        return tabuleiro;
    }

}
