package com.itau.JogoDaVelha.model;

public class ResultadoJogada {
	
	private int jogadorAtivo = 0;
    private boolean encerrado = false;
    private boolean vitoria = false;
    
 //   String[][] tabuleiro;
    
	public int getJogadorAtivo() {
		return jogadorAtivo;
	}
	public void setJogadorAtivo(int jogadorAtivo) {
		this.jogadorAtivo = jogadorAtivo;
	}
	public boolean isEncerrado() {
		return encerrado;
	}
	public void setEncerrado(boolean encerrado) {
		this.encerrado = encerrado;
	}
	public boolean isVitoria() {
		return vitoria;
	}
	public void setVitoria(boolean vitoria) {
		this.vitoria = vitoria;
	}
//	public String[][] getTabuleiro() {
//		return tabuleiro;
//	}
//	public void setTabuleiro(String[][] tabuleiro) {
//		this.tabuleiro = tabuleiro;
//	}
	
    
}
