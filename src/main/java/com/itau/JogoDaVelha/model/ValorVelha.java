package com.itau.JogoDaVelha.model;

public enum ValorVelha {
	VAZIO(" "),
	X("X"),
	O("O");
	
	String valor;
	
	ValorVelha(String valor){
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}
}
