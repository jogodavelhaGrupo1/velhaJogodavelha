package com.itau.JogoDaVelha.model;

public class JogoVelha {
	
	 private TabuleiroVelha tabuleiroVelha = new TabuleiroVelha();
	 private ValorVelha[] valores = {ValorVelha.X, ValorVelha.O};
	 private ResultadoJogada resultado = new ResultadoJogada();
	 
	 private int jogadorAtivo = 0;
	 private boolean encerrado = false;
	 private boolean vitoria = false;
	 
	 public JogoVelha(){
	 }
	 
	 public void jogar(int x, int y){
		 
	     if(encerrado){
	    	 return;
	     }
	     
	     resultado.setJogadorAtivo(jogadorAtivo);

	     ValorVelha valorDaVez = valores[jogadorAtivo];

	     boolean sucesso = tabuleiroVelha.setCasa(x, y, valorDaVez);
	   
	     if(!sucesso){
	         return;
	     }

	     vitoria = tabuleiroVelha.verificarVitoria();
	     boolean velha = tabuleiroVelha.verificarVelha();

	     if(vitoria || velha){
	         encerrado = true;
	         return;
	     }

	     if(jogadorAtivo == 0){
	        jogadorAtivo = 1;
         }else{
        	 jogadorAtivo = 0;
	     }
	  }
	 
	  public String[][] getCasas(){
		  return tabuleiroVelha.getCasas();
	  }
	 
	  public boolean isEncerrado(){
	      return encerrado;
	  }

	  public boolean isVitoria(){
	      return vitoria;
	  }

	  public int getJogadorAtivo(){
	      return jogadorAtivo;
	  }
	  
	  public ResultadoJogada getResultadoJogada() {
		  resultado.setEncerrado(encerrado);
		  resultado.setVitoria(vitoria);
//		  resultado.setTabuleiro(getCasas());
		  
		  return resultado;
	  }

}
